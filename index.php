<?php

function makeCal($year, $month) {

    $firstDayOfMonth = date('N',mktime (0, 0, 0, $month, 1, $year));
    $day = - ($firstDayOfMonth - 2);
    $cal = [];

    for ($y = 0; $y < 6; $y++) {

        $row = [];
        $notEmpty = false;

        for ($x = 0; $x < 7; $x++, $day++) {

            if (checkdate($month, $day, $year)) {
                $row[] = $day;
                $notEmpty = true;
            } else {
                $row[] = "";
            }
        }
        if (!$notEmpty) break;
        $cal[] = $row;
    }
    return $cal;
}

function getMonthName($month){
    return date('F',mktime(0, 0, 0, $month));
}

function getYear($year){
    return date('Y', mktime(0, 0, 0, 1,1, $year));
}

// Get current date and make calendar array
$now = getdate();
$cal = makeCal($now['year'], $now['mon']);

require_once 'view.php';
