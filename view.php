<!DOCTYPE html>
<html>
<head>
    <title>Calendar</title>
    <meta charset='utf-8'>
</head>
<body>
<table border='1' cellpadding="5" cellspacing="0">
    <caption><?= getMonthName($now['mon']) .' '. getYear($now['year'])?></caption>
    <tr>
        <td>Mon</td>
        <td>Tue</td>
        <td>Wed</td>
        <td>Thu</td>
        <td>Fri</td>
        <td>Sat</td>
        <td style="color:red">Sun</td>
    </tr>
    <?php foreach ($cal as $row) :?>
        <tr>
            <?php foreach ($row as $weekDay => $day) :?>
                <td style="<?= $weekDay == 6 ? 'color:red' : '' ?>">
                    <?= $day ? $day : " " ?>
                </td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
</table>
</body>
</html>